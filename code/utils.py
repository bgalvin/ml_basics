import pandas as pd
import numpy as np

def generate_data():
    data =  pd.read_csv('http://www-bcf.usc.edu/~gareth/ISL/Advertising.csv', index_col=0)
    x = data.values[:,0:3]
    y = data.values[:,3]
    return x,y


def normalize(X):
    mu = np.mean(X, axis=0)
    Smin = np.amin(X, axis=0)
    Smax = np.amax(X, axis=0)
    x = (X - mu) / (Smax - Smin)
    return x