import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification, make_gaussian_quantiles
from scipy.optimize import minimize
from random import randint
from itertools import product
from utils import normalize
from scipy.linalg import norm
import scipy.io as sio


###############
data_from_matlab = sio.loadmat('ex3data1')
x1 = data_from_matlab['X']
x2 = np.hstack((np.atleast_2d(np.ones(x1.shape[0])).T, x1))

y_fm = data_from_matlab['y']
vec_y = range(1,11)
y1 = (vec_y == y_fm).astype(int)


m = x2.shape[0]


def cost_func(_betas, x, y, lam):
    """
    Cost function for betas, feature x, and targets y.  lam specifies the strength of regularization
    :param _betas:
    :param x:
    :param y:
    :param lam:
    :return:
    """
    pred_probs = np.dot(x, _betas)
    log_lik = (-y) * np.log(sigmoid_func(pred_probs)) - (1 - y) * np.log(
        1 - sigmoid_func(pred_probs))  # Remember not to regularize the intercept term i.e. only betas[1:]
    return np.mean(log_lik) + (lam / (2.0 * m)) * np.dot(_betas[1:], _betas[1:])


def cost_func_der(_betas, x, y, lam):
    """
    Returns Nx1 vector of partial derivatives (of the cost function) where N is number of features.
    :param _betas:
    :param x:
    :param y:
    :param lam:
    :return:
    """
    beta_reg = _betas
    beta_reg[0] = 0.0
    pred_probs = np.dot(x, _betas)
    sigmoid_results = sigmoid_func(pred_probs)
    sigmoid_error = sigmoid_results - y
    return (1.0 / m) * (np.dot(x.T,sigmoid_error)) + (float(lam) / m) * (beta_reg)


def sigmoid_func(x):
    """for all entries in x multiply by the appropriate beta and sum"""
    return 1.0 / (1.0 + np.exp(-x) + 1e-6)  # small conditioning term


def predict(_betas, x):
    """
    given a set of logistic parameters and test data x returns the logistic regression prediction
    Predicts new y given parameter vector betas and features x.
    :param _betas:
    :param x:
    :return:
    """
    pred_probs = sigmoid_func(betas, x)
    binary_classification = [1 if i >= .5 else 0 for i in pred_probs]
    return binary_classification


def fit(x, y, alpha=.1, iters=5000, lam=1, show_min_plot=False):
    """
    Given features x and targets Y finds appropriate parameters to minimize the cost function.  This function uses both
    gradient decent and scipys built in minimization methods for comparison.  It returns both
    :param x:
    :param y:
    :param alpha:
    :param iters:
    :param lam:
    :return:
    """
    betas_from_gd = np.zeros((x.shape[1], y.shape[1]))
    betas2 = np.zeros(x.shape[1])
    energy = np.zeros(iters)

    for i in range(iters):
        beta_updates = cost_func_der(betas_from_gd, x, y, lam)
        betas_from_gd = betas_from_gd - alpha * beta_updates
        energy[i] = cost_func(betas_from_gd, x, y, lam)

    if show_min_plot:
        # plt.figure()
        plt.plot(energy)
        plt.ylabel('Cost function value')
        plt.xlabel('Iteration')
        plt.title('Cost function during minimization')
        plt.draw()

    betas_from_scipymin = minimize(cost_func, np.zeros(x.shape[1]), (x, y, lam), jac=cost_func_der)

    return betas_from_gd, betas_from_scipymin.x


def map_to_higher_dim(x, dim):
    """
    Maps NxM matrix x using all polynomial combos up to dim
    :param x:
    :param dim:
    :return:
    """
    x = np.atleast_2d(x)
    exp_range = product(range(dim), repeat=x.shape[1])
    pwrs = [i for i in exp_range]
    out = np.zeros((x.shape[0], len(pwrs)))

    for i in range(out.shape[0]):
        out[i, :] = np.product(np.power(x[i, :], pwrs), 1)

    return out


def quadratic_map(x, dim):
    x = np.atleast_2d(x)
    out = np.array()
    for i in range(1, dim):
        for j in range(0, i):
            # np.vstack(out,x1[:,0])
            print ('x1^' + str(i - j) + ' * x2^' + str(j))


###############

lamb = 25
betas, betas2 = fit(x2, y1, lam=lamb)

# using sklearn
lr = linear_model.LogisticRegression(fit_intercept=True, C=.1)
lr.fit(x1, y1)

print (betas)
print ('acc:' + str(sum(predict(betas, x2) == y1) / float(y1.shape[0])))
print (betas2)
print ('acc:' + str(sum(predict(betas2, x2) == y1) / float(y1.shape[0])))
print (str(lr.coef_))
print ('acc:' + str(sum(lr.predict(x1) == y1) / float(y1.shape[0])))

forplot = np.linspace(np.min(x2[:, 1]), np.max(x2[:, 1]))

plt.figure()

# for i in range(numb_features - 1):
#     l1_plot = plt.subplot(1, numb_features - 1, i + 1)
#     l1_plot.scatter(x2[:, 1], x2[:, i + 2], marker='o', c=y1, lw=0, cmap='cool')
#     l1_plot.plot(forplot, (- 1.0 / betas[2]) * (betas[1] * forplot + betas[0]), color='red')
#     l1_plot.plot(forplot, (- 1.0 / betas2[2]) * (betas2[1] * forplot + betas2[0]), color='green')
#     l1_plot.plot(forplot, (- 1.0 / lr.coef_[0][1]) * (lr.coef_[0][0] * forplot), color='blue')
#     plt.ylim(np.min(x2[:, 2]), np.max(x2[:, 2]))
#
# plt.ylabel('x_2')
# plt.xlabel('x_1')
# plt.title('Decision boundary from various methods')
# plt.legend(['Grad. descent', 'Scipy min.', 'Scipy Log. Reg.'])
# plt.draw()
#
# hi_dim_x = map_to_higher_dim(x1, numb_mapped_features)
# hi_dim_betas, hi_dim_betas2 = fit(hi_dim_x, y1, lam=lamb)
#
# delta = 0.025
# x = np.linspace(np.min(x2[:, 1]), np.max(x2[:, 1]))
# y = np.linspace(np.min(x2[:, 2]), np.max(x2[:, 2]))
# z = np.zeros((len(x), len(y)))
# for arr_x, i in enumerate(x):
#     for arr_y, j in enumerate(y):
#         z[arr_y, arr_x] = np.dot(map_to_higher_dim([i, j], numb_mapped_features), hi_dim_betas)
#
# print norm(hi_dim_betas2)
#
# plt.figure()
# CS = plt.contour(x, y, z, [0, 1, -1])
# plt.clabel(CS, inline=1, fontsize=10)
# plt.title('Nonlinear Decision Boundary: lambda = %f' % lamb)
# plt.scatter(x2[:, 1], x2[:, 2], marker='o', c=y1, lw=0, cmap='cool')
# plt.show()
