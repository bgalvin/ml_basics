import numpy as np
from sklearn import linear_model
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from scipy.optimize import minimize


class logistic_regression():
    """
    Simple multivariate logistic regression using gradient descent. Param fitting happens when
    the instance is created.
    """

    def __init__(self, _x, _y, _iters=5000, _alpha=.1):

        self.iters = _iters
        self.alpha = _alpha

        # Bias term does not work in this implimentation, will include in next iteration
        self.fi = True
        if self.fi:
            self.x = np.hstack((np.atleast_2d(np.ones(_x.shape[0])).T, _x))
        else:
            self.x = _x

        self.y = _y
        self.beta_updates = np.zeros(self.x.shape[1])
        self.m = self.x.shape[0]

        # solve using gradient decent
        self.betas = np.zeros(self.x.shape[1])
        self.energy = np.zeros(self.iters)

        # Grad of cost function is partial dir applied to each feature.
        deltaB_k = lambda betas, j: (self.alpha / self.m) * np.sum((self.h0(betas) - self.y) * self.x[:, j])
        deltaB = lambda betas: [deltaB_k(betas, j) for j in range(betas.shape[0])]

        for i in range(self.iters):
            self.beta_updates = deltaB(self.betas)
            self.betas = self.betas - self.beta_updates
            self.energy[i] = self.j0(self.betas)

        self.betas2 = minimize(self.j0, np.zeros(self.x.shape[1]))

        plt.plot(self.energy)
        plt.show()

    def fit(self, new_x):

        # calclulate betas using normal eqn

        new_x = np.atleast_2d(new_x)
        if self.fi:
            return self.betas.dot(np.hstack((np.atleast_2d(np.ones(new_x.shape[0])).T, new_x)).T)
        else:
            return self.betas.dot(new_x.T)

    def j0(self, _betas):
        """Cost function making use of h0().  Does not store any instance vars"""
        return -(1.0 / self.m) * np.sum(self.y * np.log(self.h0(_betas)) + (1 - self.y) * np.log(1 - self.h0(_betas)))

    def h0(self, _betas):
        """for all entries in x multiply by the appropriate beta and sum.  Does not store instance vars"""
        return 1 / (1 + np.exp(-np.sum(self.x * _betas, 1)))


if __name__ == "__main__":
    numb_features = 2
    x, y = make_classification(n_features=numb_features, n_redundant=0, n_informative=1,
                               n_clusters_per_class=1, random_state=1)

    for i in range(numb_features - 1):
        l1_plot = plt.subplot(3, 2, i + 1)
        l1_plot.scatter(x[:, 0], x[:, i + 1], marker='x', c=y)

    plt.show()

    # using my model
    my_lr = logistic_regression(x, y)
    print('My Coefs     : ' + str(my_lr.betas))

    # using sklearn
    lr = linear_model.LogisticRegression(fit_intercept=True, C=1e100)
    lr.fit(x, y)
    print('sklearn coefs: ' + str(lr.coef_))
