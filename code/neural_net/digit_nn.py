import scipy.io as sio
import numpy as np

def sigmoid_grad(x):
    return np.exp(x) / ((1+np.exp(x))**2)

def sigmoid_func(x):
    return 1.0 / (1.0 + np.exp(-x) + 1e-6)  # small conditioning term

data_from_matlab = sio.loadmat('ex4data1')
weights_from_matlab = sio.loadmat('ex4weights')

x = data_from_matlab['X']
y = data_from_matlab['y']

y_fm = data_from_matlab['y']

vec_y = range(1,11)
y1 = (vec_y == y_fm).astype(int)
m = y1.shape[0]


theta1 = weights_from_matlab['Theta1']
theta2 = weights_from_matlab['Theta2']



######### FORWARDPROP #########

## LAYER 1 (INPUT)
a1 = np.hstack((np.atleast_2d(np.ones(x.shape[0])).T, x))


## LAYER 2 (HIDDEN 1)
#z2 = np.dot(a1, theta1.T)
z2 = np.dot(x,theta1[:,1:].T) + np.atleast_2d(theta1[:,0].T)

a2 = sigmoid_func(z2)
a2p = np.hstack((np.atleast_2d(np.ones(a2.shape[0])).T, a2))

## LAYER 3 (HIDDEN 2)
#z3 = np.dot(a2p, theta2.T)
z3 = np.dot(a2,theta2[:,1:].T) + np.atleast_2d(theta2[:,0].T)
a3 = sigmoid_func(z3)

out =  np.argmax(a3, axis=1)

print(np.sum(y.flatten() == (out+1)) / np.float(y.shape[0]))



######### COST FUNC #########

## costFunc - NO REG

toSum = -y1*np.log(a3)-(1-y1)*np.log(1-a3)
costVal = np.sum(toSum)/m
print(costVal)

## costFunc - REG
lam = 1
reg_cosVal = costVal + (lam/(2*m))*(np.sum( theta1[:,1:]**2) + np.sum(theta2[:,1:]**2))
print(reg_cosVal)

## randomizing thetas
e_enit = .12
t1 = np.random.random(theta1.shape)/2
t2 = np.random.random(theta2.shape)/2



######### BACKPROP #########

# d3 is the difference between a3 and the y_matrix. The dimensions are the same as both, (m x r).
delta3 = a3-y1

# d2 is tricky. It uses the (:,2:end) columns of Theta2. d2 is the product of d3 and Theta2(no bias), then element-wise
#  scaled by sigmoid gradient of z2. The size is (m x r) ⋅ (r x h) --> (m x h). The size is the same as z2, as must be.
delta2 = np.dot(delta3, theta2[:,1:]) * sigmoid_grad(z2)

# Delta1 is the product of d2 and a1. The size is (h x m) ⋅ (m x n) --> (h x n)
Delta1 = np.dot(delta2.T,a1)

# Delta2 is the product of d3 and a2. The size is (r x m) ⋅ (m x [h+1]) --> (r x [h+1])
Delta2 = np.dot(delta3.T,a2p)

# Theta1_grad and Theta2_grad are the same size as their respective Deltas, just scaled by 1/m.
Theta1_grad_unReg = Delta1/m
Theta2_grad_unReg = Delta2/m

# grad reg but only reg the non bias terms (aka everything but the first column, set that to zeros)
Theta1_grad_regTerm = (lam/m)*theta1
Theta1_grad_regTerm[:,0] = 0
Theta2_grad_regTerm = (lam/m)*theta2
Theta2_grad_regTerm[:,0] = 0


full_Theta1_grad = Theta1_grad_unReg+Theta1_grad_regTerm
full_Theta2_grad = Theta2_grad_unReg+Theta2_grad_regTerm

######### GRAD DESCENT #########

theta1 = theta1 - .1*full_Theta1_grad
theta2 = theta2 - .1*full_Theta2_grad



######### FORWARDPROP #########

## LAYER 1 (INPUT)
a1 = np.hstack((np.atleast_2d(np.ones(x.shape[0])).T, x))


## LAYER 2 (HIDDEN 1)
#z2 = np.dot(a1, theta1.T)
z2 = np.dot(x,theta1[:,1:].T) + np.atleast_2d(theta1[:,0].T)

a2 = sigmoid_func(z2)
a2p = np.hstack((np.atleast_2d(np.ones(a2.shape[0])).T, a2))

## LAYER 3 (HIDDEN 2)
#z3 = np.dot(a2p, theta2.T)
z3 = np.dot(a2,theta2[:,1:].T) + np.atleast_2d(theta2[:,0].T)
a3 = sigmoid_func(z3)

out =  np.argmax(a3, axis=1)



######### COST FUNC #########

## costFunc - NO REG

toSum = -y1*np.log(a3)-(1-y1)*np.log(1-a3)
costVal = np.sum(toSum)/m
print(costVal)

## costFunc - REG
lam = 1
reg_cosVal = costVal + (lam/(2*m))*(np.sum( theta1[:,1:]**2) + np.sum(theta2[:,1:]**2))
print(reg_cosVal)