import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import minimize
from scipy.linalg import norm
from collections import namedtuple
from tqdm import tqdm
import numba



class BackpropNeuralnet():
    """

    """
    # connectedLayers = []
    # nodeLayers = []
    # cL = namedtuple('connectiveLayers', ['x', 'y'], verbose=True)
    # connectiveLayers = namedtuple('Connective Layers', ['x', 'y'], verbose=True)
    # nodeLayers = namedtuple('Node layers', ['a', 'z', 'b'], verbose=True)

    def __init__(self, layers, activations):
        """
        define network architecture, do not define X and Y training data.  Define activation function (maybe a function
        passed in by the user?

        Takes in a network architecture information as tuples ex: "layers = (400,50,10)  acts = ('sigmoid','sigmoid')".
        Will maybe include the ability to pass in a custom activation func and func derivative.

        In the above example we define a network with 400 inputs, 50 hidden units and 10 output units
        with sigmoid activation.

        :return:
        """

        # TODO later this will be selectable.  Need to support user inputed funcs and func ders
        self.aF = self.sigmoid_func
        self.DaF = self.sigmoid_func_grad


        self.layerArch = layers
        self.actFuncArch = activations

        #some helper vars
        self.layerLength = len(self.layerArch)
        self.finalLayerIdx = self.layerLength-1

        # 'a' is a list of the activation of each node in the layer
        # 'z' is a list of the weighted sum (pre-activation) of the nodes feeding into the current layer.  For activation
        #    function f in a layer, a=f(z)
        #    NOTE: z[0] will not really exist because the inputs are not run through the activation func
        self.nodeLayers = {'a': [], 'z': []}

        # 'W' is a matrix of weights from layer i to layer i+1 (for example for an input of 4 units to a hidden layer of 7
        # that is a[0] is a matrix of 4, then W[0] would be a 7x3 connective matrix such that z[1] would be np.dot(a[0], W[0].T)
        # 'd' is the error matrix
        # 'b' is matrix of the bias terms for that layer will be a #samples x 1 sized matrix
        self.connectiveLayers = {'W': [], 'b': [], 'd': [], 'dW': [], 'db': []}

        #
        #              W[0]     f()    W[1]
        #       f()      / z[1] -> a[1] \       f()
        #   z[0] -> a[0] - z[1] -> a[1] -  z[2] -> a[2]
        #                \ z[1] -> a[1] /
        #           b[0] /         b[1] /


        # TODO: before we construct weight matrices make sure that the input layers and activations verified
        # TODO: better explanation of this weight creation

        # for each connective layer we want to create a random weight matrix that can be easily dotted with its
        # neighboring weight layers (which haven't been created yet because we dont know the number of samples)
        # make sure they are random to avoid symmetry locking
        for i in range(self.finalLayerIdx):
            self.connectiveLayers['W'].append(self.randomWeightIni(layers[i + 1], layers[i]))
            self.connectiveLayers['b'].append( self.randomWeightIni(layers[i + 1], 1) )
            # TODO do we need to ini the delta layers with random or just zeros?
            self.connectiveLayers['d'].append( self.randomWeightIni(layers[i + 1], 1) )
            self.connectiveLayers['dW'].append( self.randomWeightIni(layers[i + 1], 1) )
            self.connectiveLayers['db'].append( self.randomWeightIni(layers[i + 1], 1) )


    @numba.jit
    def fit(self, x, y, l2reg = 1, lamb=.001, iters=2000, learning_rate = .7):
        """
        :param x: NxM matrix (N samples with M features)
        :param y: NxK matrix (N samples with K possible output values).  Each K long output should be binary with a 1
        corresponding to the correct class.  For example if a sample is part of class 2 then its vector would be [0 0 1]
        :return:
        """
        self.N = x.shape[0]
        self.M = x.shape[1]
        self.K = y.shape[1]

        self.x = x
        self.y = y

        self.reg = l2reg
        self.learningRate = learning_rate
        self.lamb = lamb

        # TODO: assert that x and y have the correct dims given by the doc string, and the NN current architecture
        # something like assert self.N == y.shape[0], 'Error vars contain diff # of samples: %s and %s.' % (`y`, `x`)

        # the first activation layer is simply the input
        self.nodeLayers['a'].append(x)
        self.nodeLayers['z'].append(x)

        # now starting from a[1] we make the hidden units and output, these will be zeros for now and will be filled in
        # by forward propagation
        for i in range(1, self.layerLength):
            self.nodeLayers['a'].append(np.zeros((self.N, self.layerArch[i])))
            self.nodeLayers['z'].append(np.zeros((self.N, self.layerArch[i])))

        # forward prop the NN to prepare for grad decent
        self.forward_prop()

        self.energy = []

        # Now we do our grad descent iterations, backprob will update the betas
        for i in tqdm(range(iters)):
            self.back_prop()
            self.forward_prop()
            curEng = self.cost_func()
            self.energy.append(curEng)

        plt.plot(self.energy)
        plt.show()
        # start iteration for grad decent


    def cost_func(self):
        """
        make use of forward prop to recalculate activations, then calc cost function over outputs and weights
        :return:
        """

        # TODO pep8 these vars!
        finalActLayer = self.nodeLayers['a'][self.finalLayerIdx]

        if self.reg:
            lam = 1
            toSum = -self.y * np.log(finalActLayer) - (1-self.y) * np.log(1-finalActLayer)
            costVal = np.sum(toSum)/ self.N
            costVal += (lam/(2*self.N))* np.sum([np.sum(weightLayer**2) for weightLayer in self.connectiveLayers['W']])
        else:
            toSum = -self.y * np.log(finalActLayer) - (1-self.y) * np.log(1-finalActLayer)
            costVal = np.sum(toSum) / self.N

        return costVal


    @numba.jit
    def forward_prop(self):
        """
        :return:
        """

        # move through the layers propagating weights though NN, bias term is added separably
        # for l in layers
        #   z(l+1) = W(l) dot a(l) + b(l)
        #   a(l+1) = activationFunc( z(l+1) )

        for i in range(1, self.layerLength):
            self.nodeLayers['z'][i] = np.dot(self.nodeLayers['a'][i-1], self.connectiveLayers['W'][i-1].T) \
                                     + self.connectiveLayers['b'][i-1].T
            self.nodeLayers['a'][i] = self.aF(self.nodeLayers['z'][i])

    @numba.jit
    def back_prop(self):
        """

        :return:
        """

        #TODO Need to implement a more effective descent method from an optimization library.

        # first set the final layer, henceforth FL, to d = a[FL]-Y
        self.connectiveLayers['d'][self.finalLayerIdx-1] = self.nodeLayers['a'][self.finalLayerIdx] - self.y

        # now we move backward through the connective layers computing error terms
        for i in reversed( range(self.finalLayerIdx - 1)):
            self.connectiveLayers['d'][i] = np.dot(self.connectiveLayers['d'][i+1], self.connectiveLayers['W'][i+1]) \
                                            * self.DaF(self.nodeLayers['z'][i+1])

        # using the error terms we now calculate partial derivatives dW and dB
        for i in range(self.finalLayerIdx):
            self.connectiveLayers['dW'][i] = (np.dot(self.connectiveLayers['d'][i].T, self.nodeLayers['a'][i]))
            #self.connectiveLayers['db'][i] = (np.dot(self.connectiveLayers['d'][i].T, self.connectiveLayers['b'][i]))
            self.connectiveLayers['db'][i] = np.atleast_2d(np.sum(self.connectiveLayers['d'][i].T,axis=1))

        # grad descent
        for i in range(self.finalLayerIdx):
            self.connectiveLayers['W'][i] -= self.learningRate * ((1/self.N)*self.connectiveLayers['dW'][i] + \
                                                                  self.lamb * self.connectiveLayers['W'][i])
            self.connectiveLayers['b'][i] -= self.learningRate * ((1/self.N)*self.connectiveLayers['db'][i]).T



    def randomWeightIni(self, rows, cols, init_e=.12, randSeed = 5):
        """
        Generates a random matrix, of rows x cols for making weight matricies.
        :param rows:
        :param cols:
        :param init_e:
        :return:
        """
        np.random.seed(seed=randSeed)
        return np.random.rand(rows, cols) * (2 * init_e) - init_e

    def appendBias(self, toAppend):
        """
        Dont need this as we are handling bias units in a seperate var
        :param toAppend:
        :return:
        """
        return np.hstack((np.atleast_2d(np.ones(toAppend.shape[0])).T, toAppend))

    @numba.jit
    def sigmoid_func(self, x):
        """
        Returns the sigmoid func of x element wise
        :param x:
        :return:
        """
        return 1.0 / (1.0 + np.exp(-x) + 1e-6)  # small conditioning term

    @numba.jit
    def sigmoid_func_grad(self, x):
        """
        Returns the grad of the sigmoid func of x element wise
        :param x:
        :return:
        """
        return np.exp(x) / ((1+np.exp(x))**2)

    def unpack_weights(self):
        """

        :return:
        """
        pass

    def pack_weights(self):
        """

        :return:
        """
        pass

def show_img(index,xMat,realMat,outMat):
    plt.imshow(  np.reshape(xMat[index,:],(20,20),order='F'))
    plt.title(str(realMat[index]) + ' predicted as ' + str(outMat[index]))
    plt.set_cmap('gray')
    plt.show()

if __name__ == "__main__":

    import scipy.io as sio

    data_from_matlab = sio.loadmat('D:\GoogleDrive\Code\python\ml_base\data\ex4data1.mat')
    weights_from_matlab = sio.loadmat('D:\GoogleDrive\Code\python\ml_base\data\ex4weights.mat')

    x = data_from_matlab['X']

    y = data_from_matlab['y']
    vec_y = range(1,11)
    y1 = (vec_y == y).astype(int)
    #m = y1.shape[0]

    theta1 = weights_from_matlab['Theta1']
    theta2 = weights_from_matlab['Theta2']


    layers = (400,50,10)
    acts = ('sigmoid','sigmoid')
    myNN = BackpropNeuralnet(layers, acts)
    myNN.fit(x, y1)

    out = np.argmax(myNN.nodeLayers['a'][-1], axis=1)+1
    truth = y.flatten()
    print(np.sum(truth == out) / np.float(y.shape[0]))

    misarray = (truth == out) == False

    for i in np.flatnonzero(misarray):
        show_img(i,x,truth,out)


    # myNN.connectiveLayers['W'][0] = theta1[:,1:]
    # myNN.connectiveLayers['W'][1] = theta2[:,1:]
    #
    # myNN.connectiveLayers['b'][0] = theta1[:,0]
    # myNN.connectiveLayers['b'][1] = theta2[:,0]

    # print(myNN.cost_func())
