import numpy as np
from utils import generate_data
from sklearn import linear_model
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.datasets import make_regression


class linear_regression():
    """
    Simple multivariate linear regression with selectable solver (grad. decent or normal eqn.).  Allows for the option
    to fit an intercept term using the fit_intercept variable which defaults to true.  Param fitting happens when
    the instance is created.
    """

    def __init__(self, _x, _y, reg_type, fit_intercept=True, iters=1000, alpha=.1):

        self.iters = iters
        self.alpha = alpha

        self.fit_intercept = fit_intercept
        if self.fit_intercept:
            self.x = np.hstack((np.atleast_2d(np.ones(_x.shape[0])).T, _x))
        else:
            self.x = _x

        self.y = _y
        self.beta_updates = np.zeros(self.x.shape[1])
        self.m = self.x.shape[0]

        if reg_type == 'gd':
            # solve using gradient decent
            self.betas = np.zeros(self.x.shape[1])
            self.energy = np.zeros(self.iters)
            for i in range(self.iters):
                self.beta_updates = [(self.alpha / self.m) * np.sum((self.h0() - self.y) * self.x[:, j]) for j in
                                     range(self.betas.shape[0])]
                self.betas = self.betas - self.beta_updates
                self.energy[i] = self.j0()

            plt.plot(self.energy)
            plt.show()

        elif reg_type == 'ne':
            # solve using normal eqn
            self.betas = np.linalg.pinv(self.x.T.dot(self.x)).dot(self.x.T).dot(self.y)

        else:
            raise Exception('Solver type "' + reg_type + '" not found')

    def fit(self, new_x):

        # calculate betas using normal eqn

        new_x = np.atleast_2d(new_x)
        if self.fit_intercept:
            return self.betas.dot(np.hstack((np.atleast_2d(np.ones(new_x.shape[0])).T, new_x)).T)
        else:
            return self.betas.dot(new_x.T)

    def j0(self):
        """Cost function making use of h0().  Does not store any instance vars"""
        return np.sum((self.h0() - self.y) ** 2) / (2 * self.m)

    def h0(self):
        """for all entries in x multiply by the appropriate beta and sum.  Does not store instance vars"""
        return np.sum(self.x * self.betas, 1)


if __name__ == "__main__":
    x, y, coefs = make_regression(200, 20, 7, coef=True)

    print 'True coefs   : ' + str(coefs)

    # using my model
    my_lr = linear_regression(x, y, 'gd', _fit_intercept=False)
    print 'My Coefs     : ' + str(my_lr.betas)

    # using sklearn
    lr = linear_model.LinearRegression(fit_intercept=False)
    lr.fit(x, y)
    print 'sklearn coefs: ' + str(lr.coef_)

    print np.linalg.norm(lr.coef_ - my_lr.betas)
